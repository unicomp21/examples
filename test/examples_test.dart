import 'dart:async';
import 'package:async/async.dart';

import 'package:examples/examples.dart';
import 'package:test/test.dart';

void main() {
  group('A group of tests', () {
    group('coroutine tests', () {
      test('pub/sub', () async {
        print('test pub/sub started');
        const limit = 5;

        final bus = StreamController<String>.broadcast();

        final coroutine_subscriber_1 = () async {
          final reader = StreamQueue(bus.stream);
          for (;;) {
            final msg = await reader.next;
            if (msg.startsWith('topic1:')) {
              print('subscriber_1: ${msg}');
            }
            if (msg.startsWith('topic1: ${limit - 1}')) break; // end of stream
          }
        }();

        final coroutine_subscriber_2 = () async {
          final reader = StreamQueue(bus.stream);
          for (;;) {
            final msg = await reader.next;
            if (msg.startsWith('topic2:')) {
              print('subscriber_2: ${msg}');
            }
            if (msg.startsWith('topic2: ${limit - 1}')) break; // end of stream
          }
        }();

        final coroutine_publisher_1 = () async {
          for (var i = 0; i < limit; i++) {
            bus.add('topic1: ${i}');
            bus.add('topic2: ${i}');
          }
        }();

        await Future.wait([
          coroutine_subscriber_1,
          coroutine_subscriber_2,
          coroutine_publisher_1
        ]);

        print('test pub/sub done');
      });
    });
  });
}
